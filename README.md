# i3wmNord

Nord Theme inspired i3wm config

### Touchpad settings
put 90-touchpad.conf in /etc/X11/xorg.conf.d/

### Icon Pack
NordArc -> https://github.com/robertovernina/NordArc

### Theme
Nordic Darker -> https://www.gnome-look.org/p/1267246

### Cursor Theme
Volantes Cursors -> https://www.gnome-look.org/p/1356095
